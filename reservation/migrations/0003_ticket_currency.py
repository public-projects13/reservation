# Generated by Django 3.1.4 on 2020-12-15 18:37

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('reservation', '0002_auto_20201215_1830'),
    ]

    operations = [
        migrations.AddField(
            model_name='ticket',
            name='currency',
            field=models.CharField(choices=[('EUR', 'Euro'), ('PLN', 'Złoty')], default='EUR', max_length=3, verbose_name='Waluta'),
        ),
    ]
