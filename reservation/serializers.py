from django.utils import timezone
from rest_framework import serializers

from reservation import models


class EventSerializer(serializers.ModelSerializer):
	date = serializers.DateTimeField(format='%d-%m-%Y %H:%M')
	available_tickets = serializers.ReadOnlyField()

	class Meta:
		model = models.Event
		fields = (
			'name',
			'date',
			'available_tickets'
		)


class TicketSerializer(serializers.ModelSerializer):
	ticket_type = serializers.CharField(
		source='get_ticket_type_display',
		default='',
		read_only=True
	)
	available = serializers.ReadOnlyField()

	class Meta:
		model = models.Ticket
		fields = (
			'id',
			'ticket_type',
			'amount',
			'sold',
			'cost',
			'currency',
			'available'
		)


class ReservationSerializer(serializers.ModelSerializer):
	reservation_no = serializers.CharField(read_only=True)

	class Meta:
		model = models.Reservation
		fields = (
			'id',
			'reservation_no',
			'ticket',
			'quantity',
			'first_name',
			'last_name',
			'email'
		)

	def validate_ticket(self, ticket):
		if ticket.sold_out:
			raise serializers.ValidationError('Brak dostępnych biletów')
		quantity = self.initial_data.get('quantity')
		if ticket.sold + int(quantity) > ticket.amount:
			raise serializers.ValidationError(
				f'Zostało {ticket.available} dostępnych biletów')
		return ticket

	def create(self, validated_data):
		reservation_id = models.Reservation.objects.count()
		validated_data['reservation_no'] = \
			f'R{timezone.now().month}{timezone.now().year}{reservation_id + 1}'
		instance = super(ReservationSerializer, self).create(validated_data)
		instance.ticket.sold += instance.quantity
		instance.ticket.save(update_fields=['sold'])
		return instance


class ReservationDetailSerializer(serializers.ModelSerializer):
	status = serializers.CharField(
		source='get_status_display',
		default=''
	)
	ticket = serializers.CharField()

	class Meta:
		model = models.Reservation
		fields = (
			'status',
			'reservation_no',
			'ticket',
			'quantity',
			'first_name',
			'email',
		)
