from django.db import models

from reservation import managers, payment


class Event(models.Model):
    name = models.CharField(
        verbose_name='Nazwa',
        max_length=255
    )
    date = models.DateTimeField(
        verbose_name='Data wydarzenia'
    )
    slug = models.SlugField(
        verbose_name='slug',
        unique=True
    )

    objects = models.Manager()
    incoming_event = managers.IncomingEventManager()

    class Meta:
        verbose_name = "Wydarzenie"
        verbose_name_plural = "Wydarzenia"

    def __str__(self):
        return self.name

    @property
    def available_tickets(self):
        for ticket in self.tickets.all():
            if not ticket.sold_out:
                return True
        return False

    def ticket_sum(self):
        ticket_amount = 0
        ticket_available = 0
        for ticket in self.tickets.all():
            ticket_amount += ticket.amount
            ticket_available += ticket.available

        return ticket_amount, ticket_available

    @classmethod
    def queryset_date_range(cls, start_date, end_date):
        if start_date and end_date:
            return cls.objects.filter(date__range=(start_date, end_date))
        elif start_date and not end_date:
            return cls.objects.filter(date__gte=start_date)
        elif end_date and not start_date:
            return cls.objects.filter(date__lte=end_date)
        else:
            return cls.objects.all()

    @classmethod
    def reserved_ticket_statistic(cls, start_date, end_date):
        queryset = cls.queryset_date_range(start_date, end_date)

        result = []
        for obj in queryset:
            ticket_all, available = obj.ticket_sum()
            result.append({
                'name': obj.name,
                'date': obj.date,
                'ticket_all': ticket_all,
                'ticket_available': available
            })

        return result

    @classmethod
    def extended_reserved_ticket_statistic(cls, start_date, end_date):
        queryset = cls.queryset_date_range(start_date, end_date)

        result = []
        for obj in queryset:
            tickets_stats = []
            for ticket in obj.tickets.all():
                tickets_stats.append({
                    'type': ticket.get_ticket_type_display(),
                    'ticket_all': ticket.amount,
                    'ticket_sold': ticket.sold,
                    'ticket_available': ticket.available
                })

            result.append({
                'name': obj.name,
                'date': obj.date,
                'tickets': tickets_stats
            })

        return result


class Ticket(models.Model):
    REGULAR = 10
    PREMIUM = 20
    VIP = 30

    TICKET_TYPE = (
        (REGULAR, 'Normalny'),
        (PREMIUM, 'Premium'),
        (VIP, 'VIP'),
    )

    CURRENCY = (
        ('EUR', 'Euro'),
        ('PLN', 'Złoty')
    )

    event = models.ForeignKey(
        to='Event',
        verbose_name='Wydarzenie',
        related_name='tickets',
        on_delete=models.CASCADE
    )
    ticket_type = models.PositiveSmallIntegerField(
        verbose_name='Typ biletu',
        choices=TICKET_TYPE
    )
    amount = models.PositiveIntegerField(
        verbose_name='Ilość biletów'
    )
    sold = models.PositiveIntegerField(
        verbose_name='Ilość sprzedanych biletów',
        default=0
    )
    currency = models.CharField(
        verbose_name='Waluta',
        max_length=3,
        choices=CURRENCY,
        default='EUR'
    )
    cost = models.DecimalField(
        verbose_name='Koszt biletu',
        max_digits=6,
        decimal_places=2
    )

    class Meta:
        verbose_name = 'Bilet na wydarzenie'
        verbose_name_plural = 'Bilety na wydarzenie'

    def __str__(self):
        return f'Bilet {self.get_ticket_type_display()} dla wydarzenia ' \
               f'{self.event}'

    @property
    def sold_out(self):
        if self.amount == self.sold:
            return True
        return False

    @property
    def available(self):
        return self.amount - self.sold


class Reservation(models.Model):
    NEW = 10
    PAID = 20
    CANCELED = 30

    STATUSES = (
        (NEW, 'Nowy'),
        (PAID, 'Opłacony'),
        (CANCELED, 'Anulowany'),
    )

    reservation_no = models.CharField(
        verbose_name='Numer rezerwacji',
        max_length=25,
        unique=True
    )
    status = models.IntegerField(
        verbose_name='Status Rezerwacji',
        choices=STATUSES,
        default=NEW
    )
    ticket = models.ForeignKey(
        to='Ticket',
        verbose_name='Bilet na wydarzenie',
        on_delete=models.PROTECT
    )
    quantity = models.PositiveSmallIntegerField(
        verbose_name='Ilość biletów'
    )
    add_date = models.DateTimeField(
        verbose_name='Data dodania',
        auto_now_add=True
    )
    first_name = models.CharField(
        verbose_name='Imię kupującego',
        max_length=50
    )
    last_name = models.CharField(
        verbose_name='Nazwisko kupującego',
        max_length=50
    )
    email = models.EmailField(
        verbose_name='Email kupującego',
    )

    class Meta:
        verbose_name = 'Rezerwacja'
        verbose_name_plural = 'Rezerwacje'

    def __str__(self):
        return f'Rezerwacja biletu {self.ticket}'

    @property
    def total_cost(self):
        return self.ticket.cost * self.quantity

    def payment(self):
        gateway = payment.PaymentGateway()

        try:
            result = gateway.charge(
                amount=self.total_cost,
                token='test_token',
                currency=self.ticket.currency
            )
            self.status = self.PAID
            self.save(update_fields=['status'])
            return True, f'Zapłacono {result.amount} {result.currency}'
        except Exception as e:
            return False, e.args

