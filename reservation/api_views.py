from rest_framework import viewsets, mixins, status
from rest_framework.decorators import action
from rest_framework.response import Response

from reservation import models, serializers


class EventViewSet(viewsets.GenericViewSet, mixins.ListModelMixin):
	queryset = models.Event.incoming_event.all()
	serializer_class = serializers.EventSerializer
	lookup_field = 'slug'

	@action(methods=['GET'], detail=True, url_path='tickets')
	def tickets(self, request, *args, **kwargs):
		obj = self.get_object()
		serializer = serializers.TicketSerializer(obj.tickets.all(), many=True)

		return Response(data=serializer.data, status=status.HTTP_200_OK)

	@action(methods=['POST'], detail=False, url_path='sold-ticket')
	def sold_ticket_statistics(self, request, *args, **kwargs):
		result = models.Event.reserved_ticket_statistic(
			start_date=request.data.get('start_date'),
			end_date=request.data.get('end_date')
		)
		return Response(data=result, status=status.HTTP_200_OK)

	@action(methods=['POST'], detail=False, url_path='sold-ticket-extended')
	def extended_sold_ticket_statistics(self, request, *args, **kwargs):
		result = models.Event.extended_reserved_ticket_statistic(
			start_date=request.data.get('start_date'),
			end_date=request.data.get('end_date'))
		return Response(data=result, status=status.HTTP_200_OK)


class ReservationViewSet(viewsets.GenericViewSet, mixins.CreateModelMixin):

	queryset = models.Reservation.objects.all()
	serializer_class = serializers.ReservationSerializer
	lookup_field = 'reservation_no'

	@action(methods=['PATCH'], detail=True, url_path='payment')
	def payment(self, request, *args, **kwargs):
		obj = self.get_object()
		done, result = obj.payment()

		if done:
			return Response(
				data={'message': result},
				status=status.HTTP_200_OK
			)
		else:
			return Response(
				data={'error': result},
				status=status.HTTP_402_PAYMENT_REQUIRED
			)

	@action(methods=['POST'], detail=False, url_path='info')
	def reservation_info(self, request, *args, **kwargs):
		try:
			reservation = models.Reservation.objects.get(
				reservation_no=request.data.get('reservation_no'),
				email=request.data.get('email')
			)
			serializer = serializers.ReservationDetailSerializer(
				instance=reservation
			)
			return Response(data=serializer.data, status=status.HTTP_200_OK)
		except models.Reservation.DoesNotExist:
			return Response(data={}, status=status.HTTP_404_NOT_FOUND)
