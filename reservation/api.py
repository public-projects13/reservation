from django.urls import path, include
from rest_framework import routers

from reservation import api_views

router = routers.SimpleRouter()
router.register(r'event', api_views.EventViewSet, basename='event')
router.register(r'reservation', api_views.ReservationViewSet,
                basename='reservation')

urlpatterns = [
    path(r'', include(router.urls))
]
