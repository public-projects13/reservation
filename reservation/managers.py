from django.db import models
from django.utils import timezone


class IncomingEventManager(models.Manager):

    def get_queryset(self):
        return super(IncomingEventManager, self).get_queryset().filter(
            date__gte=timezone.now()
        )
