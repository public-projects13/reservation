from datetime import timedelta

from django.utils import timezone

from monterail_reservation.celery import app
from reservation import models


@app.task()
def release_tickets():
	"""
	If website traffic is high change QuerySet, get 100-500 objects and run
	more workers.
	"""

	reservation_qs = models.Reservation.objects.filter(
		status=models.Reservation.NEW,
		add_date__gte=timezone.now() + timedelta(minutes=15)
	)

	for reservation in reservation_qs:
		reservation.ticket.sold -= reservation.quantity
		reservation.ticket.save(update_fields=['sold'])
		reservation.delete()
