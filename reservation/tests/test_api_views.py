from datetime import timedelta

from django.test import TestCase
from django.urls import reverse
from django.utils import timezone
from rest_framework.test import APIRequestFactory

from reservation import models, api_views


class InitTest(TestCase):
	def setUp(self) -> None:
		self.client = APIRequestFactory()
		self.old_event = models.Event.objects.create(
			name='Old Event',
			slug='old',
			date=timezone.now() - timedelta(days=10))
		self.event = models.Event.objects.create(
			name='Test Event',
			slug='test',
			date=timezone.now() + timedelta(days=10))
		self.normal_ticket = models.Ticket.objects.create(
			event=self.event,
			ticket_type=models.Ticket.REGULAR,
			amount=100,
			cost=19.99
		)
		self.premium_ticket = models.Ticket.objects.create(
			event=self.event,
			ticket_type=models.Ticket.PREMIUM,
			amount=20,
			cost=49.99
		)
		self.vip_ticket = models.Ticket.objects.create(
			event=self.event,
			ticket_type=models.Ticket.VIP,
			amount=10,
			cost=99.99
		)


class EventViewSetTest(InitTest):
	def test_list(self):
		request = self.client.get(
			reverse('event-list'),
		)
		list_view = api_views.EventViewSet.as_view({'get': 'list'})
		response = list_view(request)

		self.assertEqual(response.status_code, 200)
		self.assertEqual(len(response.data), 1)

	def test_tickets(self):
		request = self.client.get(
			reverse('event-tickets', kwargs={'slug': self.event.slug}),
		)
		tickets_view = api_views.EventViewSet.as_view(
			{'get': 'tickets'}

		)
		response = tickets_view(request, slug=self.event.slug)

		self.assertEqual(response.status_code, 200)
		self.assertEqual(len(response.data), 3)

	def test_sold_ticket_statistics(self):
		request = self.client.post(
			reverse('event-sold-ticket-statistics'),
			data={
				'start_date': timezone.now() - timedelta(days=20),
				'end_date': timezone.now() + timedelta(days=20)
			}
		)

		statistics_view = api_views.EventViewSet.as_view(
			{'post': 'sold_ticket_statistics'}
		)

		response = statistics_view(request)

		self.assertEqual(response.status_code, 200)
		self.assertEqual(len(response.data), 2)


class ReservationViewSetTest(InitTest):

	def setUp(self) -> None:
		super(ReservationViewSetTest, self).setUp()
		self.reservation = models.Reservation.objects.create(
			reservation_no=f'test',
			status=models.Reservation.NEW,
			ticket=self.normal_ticket,
			quantity=2,
			first_name='John',
			last_name='Doe',
			email='johndoe@example.com'
		)
		self.reservation_paid = models.Reservation.objects.create(
			reservation_no=f'test2',
			status=models.Reservation.PAID,
			ticket=self.premium_ticket,
			quantity=2,
			first_name='Mateusz',
			last_name='Gajewski',
			email='mg@example.com'
		)

	def test_create(self):
		request = self.client.post(
			reverse('reservation-list'),
			data={
				'ticket': self.normal_ticket.id,
				'quantity': 1,
				'first_name': 'John',
				'last_name': 'Doe',
				'email': 'johndoe@xxx.com'
			}
		)

		create_view = api_views.ReservationViewSet.as_view(
			{'post': 'create'}
		)

		response = create_view(request)
		self.assertEqual(response.status_code, 201)
		self.assertEqual(response.data.get('email'), 'johndoe@xxx.com')

	def test_payment(self):
		request = self.client.post(
			reverse('reservation-payment', kwargs={
				'reservation_no': self.reservation.reservation_no}),
		)

		payment_view = api_views.ReservationViewSet.as_view(
			{'post': 'payment'}
		)

		response = payment_view(
			request, reservation_no=self.reservation.reservation_no)

		self.assertEqual(response.status_code, 200)

	def test_reservation_info(self):
		request = self.client.post(
			reverse('reservation-reservation-info'),
			data={
				'reservation_no': self.reservation_paid.reservation_no,
				'email': self.reservation_paid.email
			}
		)

		info_view = api_views.ReservationViewSet.as_view(
			{'post': 'reservation_info'}
		)

		response = info_view(request)

		self.assertEqual(response.status_code, 200)
